# -*- coding: utf-8 -*-
"""
Created on Tue Nov 13 09:08:46 2018

@author: pawel.rakowski
"""

import os
#from itertools import izip as zip (aparently faster than original zip)
path = "C:/PAWEL/MODELLING/modpat7 tests/for gitlab"
os.chdir(path)

import flopy
import numpy as np
import matplotlib 



ml = flopy.modflow.Modflow.load("org/gv1.nam",verbose=True,check=False, exe_name="mf2005")
ml.model_ws = "run"
ml.write_input()
ml.run_model()
ml.plot()


locations = [(1,20,27),(1,30,27)]
particleids = [91,92]
exe_name="MPath7"
# exe_name="MPath7",flowmodel = ml,ml.model_ws = "test1"
particles = flopy.modpath.ParticleData(partlocs = locations,
                                       structured=True,
                                       particleids = particleids)
particles_g = flopy.modpath.ParticleGroup(particledata=particles)


mp = flopy.modpath.Modpath7("mp1", flowmodel=ml,
                            exe_name=exe_name, model_ws = "run")
mpbas = flopy.modpath.Modpath7Bas(mp, porosity=0.1)
mpsim = flopy.modpath.Modpath7Sim(mp, 
                                  simulationtype='combined',
                                  trackingdirection='backward',
                                  weaksinkoption='pass_through',
                                  weaksourceoption='pass_through',
                                  budgetoutputoption='summary',
                                  #budgetcellnumbers=[1049, 1259],
                                  #traceparticledata=[1, 1000],
                                  #referencetime=[0, 0, 0.],
                                  stoptimeoption='extend',
                                  timepointdata=[100,2.],
                                  #zonedataoption='on', zones=zones,
                                  particlegroups=particles_g)

# write modpath datasets
mp.write_input()


# run modpath
mp.run_model()
fpth = "run/mp1.mppth"
p = flopy.utils.PathlineFile(fpth,verbose = True)

p.get_maxid() #only one particle
p0 = p.get_alldata(totim = 50.,ge = True)
p0 #only one element in list, should be 2)i





mm = flopy.plot.ModelMap(model=ml)
mm.plot_ibound()
mm.plot_grid()
mm.plot_pathline(p0,layer='all', color='red',travel_time = 200) 
#this seemd to not to work, only one particle ploteed


fpth = "run/mp1.timeseries"
ts = flopy.utils.TimeseriesFile(fpth)
ts0 = ts.get_alldata()



mm = flopy.plot.ModelMap(model=ml)
mm.plot_ibound()
mm.plot_grid()
mm.plot_timeseries(ts0, layer=1,lw=0.75)
#this works fine